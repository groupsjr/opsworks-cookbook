# README

## Recipes

Click on each recipe to see more documentation.

* Lock down Apache headers and setup PHP configs

    * [beta_apache::add_headers](./beta_apache/recipes/add_headers.rb)

* Pipe logs to remote ELK server (LogStash/Kibana)

    * [beta_beats::default](./beta_beats/recipes/default.rb) - OpsWorks JSON config required
    * [beta_beats::restart](./beta_beats/recipes/restart.rb)

* Block abusive traffic (experimental)

    * [beta_fail2ban::default](./beta_fail2ban/recipes/default.rb) - OpsWorks JSON config required
    * [beta_fail2ban::default](./beta_fail2ban/recipes/default.rb)

* Setup Node dependencies

    * [beta_gulp_setup::install](./beta_gulp_setup/recipes/install.rb)

* Setup Amazon SES as default `sendmail` SMTP server

    * [beta_ses_mail::default](./beta_ses_mail/recipes/default.rb) - OpsWorks JSON config required

* Fix file permissions

    * [beta_w3tc_perms::fix](./beta_w3tc_perms/recipes/fix.rb) - may need updating based on W3TC peculiarities over time
    * [beta_wp_uploads::fix](./beta_wp_uploads/recipes/fix.rb) - fixes `uploads` permissions and symlinks across deploys
        * [beta_wp_perms::set](./beta_wp_perms/recipes/set.rb) - outdated because it does not symlink across deploys
    * [beta_drupal_tmp::set](./beta_drupal_tmp/recipes/set.rb)

## WordPress on OpsWorks

Typically, we setup the following recipes for a WordPress site on OpsWorks:

* OpsWorks Deploy:
    * [beta_apache::add_headers](./beta_apache/recipes/add_headers.rb)
    * [beta_wp_uploads::fix](./beta_wp_uploads/recipes/fix.rb)
    * [beta_w3tc_perms::fix](./beta_w3tc_perms/recipes/fix.rb) - usually don't have W3TC setup on OpsWorks (because of load balancing) unless Memcached is used with W3TC

We still use Mailgun as a WordPress plugin to send mail. However, Amazon SES is in development with this recipe:

* [beta_ses_mail::default](./beta_ses_mail/recipes/default.rb)