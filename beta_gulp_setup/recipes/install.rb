#
# Install `gulp`, which depends on `node` and `npm` being installed.
#
# Note: Some servers use `nodejs` and some use `node` as the executable name. You may need to symlink one to the other, depending on environment.
#

include_recipe "nodejs::npm"

nodejs_npm "gulp"
